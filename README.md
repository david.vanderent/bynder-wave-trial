# bynder-cypress

## Overview

This repo contains tests written for Bynder.

## Run end-to-end tests

To execute the end-to-end tests, enter the following commands:

```bash
cd e2e
docker-compose up --exit-code-from cypress
```

When the command completes, you will see test output on the console and a video of the test run will appear in the folder `e2e/cypress/integration/videos`.

Testscenario 'redirection is successful' is meant to fail, because i think i might have found an issue there. So created this scenario to show it to you.

## Test scenarios

Feature: Login and logout

Scenario: Successful login

Given I am on page https://wave-trial.getbynder.com/login/
When I enter a valid username and password
And I click the login button
Then the dashboard page /account/dashboard should show

Scenario: Successful logout

Given I am on the dashboard page /account/dashboard
When I hover over my name on top right of the page 
And I click the logout button
Then I should get back to the login page https://wave-trial.getbynder.com/login/

Scenario: Failed login

Given I am on page https://wave-trial.getbynder.com/login/
When I enter an invalid username and password
And I click the login button
Then a message of incorrect username or password should be displayed

Scenario: Redirection is successful

Given I have started my browser
When I copy this https://wave-trial.getbynder.com/login/ as URL
And I hit Enter button
Then the trial login page should be displayed
And I should be able to successfully login

Scenario: Lost password

Given I am on page https://wave-trial.getbynder.com/login/
When I click Lost password
Then https://wave-trial.getbynder.com/user/forgotPassword/ should be displayed
