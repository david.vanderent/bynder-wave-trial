
describe('Bynder Login and logout', () => {

  it('logs in successfully', () => {
/// Given I am on page https://wave-trial.getbynder.com/login/
    cy.visit('https://wave-trial.getbynder.com/login/')

/// When I enter a valid username and password
	cy.get('[name=username]')
	  .type('d.vanderent@ridder.com')
      .should('have.value', 'd.vanderent@ridder.com')
  
	cy.get('[name=password]')
	  .type('9sbQsY3wfM3j')

/// And I click the login button
	cy.contains('Login').click()

/// Then the dashboard page /account/dashboard should show
    cy.url().should('include', '/account/dashboard')

  })


  it('logs out successfully', () => {
/// Given I am on the dashboard page /account/dashboard
    cy.url().should('include', '/account/dashboard')	  
	  
/// When I hover over my name on top right of the page 
	cy.contains('David van+der+Ent').click()
	  
/// And I click the logout button
	cy.contains('Logout').click()

/// Then I should get back to the login page
	cy.contains('button', 'Login')
	cy.reload()
  })


it('login fails', () => {
///Given I am on page https://wave-trial.getbynder.com/login/
    cy.visit('https://wave-trial.getbynder.com/login/')
	
/// When I enter an invalid username and password
	cy.get('[name=username]')
	  .type('dvanderent@ridder.com')
      .should('have.value', 'dvanderent@ridder.com')
  
	cy.get('[name=password]')
	  .type('dummypass')

/// And I click the login button
	cy.contains('Login').click()

/// Then a message of incorrect username or password should be displayed
    cy.url().should('not.include', '/account/dashboard')
	cy.contains('div', 'You have entered an incorrect username or password.')
	
  })


it('redirection is successful', () => {
///Given I have started my browser
/// When I copy this https://wave-trial.bynder.com/login/ as URL
	cy.visit('https://wave-trial.bynder.com/login/')

/// And I hit Enter button
/// Then the trial login page should be displayed
  	cy.get('[title="Wave Trial"]')

/// And I should be able to successfully login
	cy.get('[name=username]')
	  .type('d.vanderent@ridder.com')
      .should('have.value', 'd.vanderent@ridder.com')
  
	cy.get('[name=password]')
	  .type('9sbQsY3wfM3j')

	cy.contains('Login').click()

    cy.url().should('include', '/account/dashboard')	
  })


it('lost password works', () => {
///Given I am on page https://wave-trial.getbynder.com/login/
	cy.visit('https://wave-trial.getbynder.com/login/')

/// When I click Lost password
	cy.contains('a', 'Lost password').click()

/// Then https://wave-trial.getbynder.com/user/forgotPassword/ should be displayed
    cy.url().should('include', '/user/forgotPassword/')	

  })
  
})
